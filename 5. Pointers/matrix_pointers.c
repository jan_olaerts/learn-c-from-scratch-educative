#include <stdio.h>
#include <stdlib.h>
 
typedef struct {
  double *data;
  int nrows;
  int ncols;
} Matrix;

void printmat(Matrix *M);
void matrixmult(Matrix *A, Matrix *B, Matrix *C);
Matrix *createMatrix(int nrows, int ncols);
void destroyMatrix(Matrix *M);

int main(int argc, char *argv[])
{
  
  Matrix *A = createMatrix(3, 2);
  //Uncomment the following code when you implement createMatrix
  //otherwise it will give a segmention fault if createMatrix
  //is not implemented correctly 

  A->data[0] = 1.2;
  A->data[1] = 2.3;
  A->data[2] = 3.4;
  A->data[3] = 4.5;
  A->data[4] = 5.6;
  A->data[5] = 6.7;
  printmat(A);
  

  Matrix *B = createMatrix(2, 3);
  B->data[0] = 5.5;
  B->data[1] = 6.6;
  B->data[2] = 7.7;
  B->data[3] = 1.2;
  B->data[4] = 2.1;
  B->data[5] = 3.3;
  printmat(B);

  Matrix *C = createMatrix(3, 3);
  matrixmult(A, B, C);
  printmat(C);

  destroyMatrix(A);
  destroyMatrix(B);
  destroyMatrix(C);
  
  return 0;
}

// your code goes below...


Matrix *createMatrix(int nrows, int ncols)
{
  Matrix *m = malloc(sizeof(Matrix));
  m -> data = malloc(nrows*ncols*sizeof(double));
  m -> nrows = nrows;
  m -> ncols = ncols;
  return m;
}

void destroyMatrix(Matrix *m)
{
  free(m -> data);
  free(m);
}

void printmat(Matrix *m)
{
  int dataSize = m -> nrows * m -> ncols;
  for (int i = 0; i < dataSize; i++)
  {
    printf("matrix.data[%d]=%.1f\n", i, m -> data[i]);
  }
  printf("\n");
}

void matrixmult(Matrix *a, Matrix *b, Matrix *c)
{
  if (a -> nrows != b -> ncols)
  {
    printf("a.nrows must be equal to b.ncols\n");
  }
  else
  {
    int count = 0;
    for (int arow = 0; arow < a -> nrows; arow++)
    {
      for (int bcol = 0; bcol < b -> ncols; bcol++)
      {
        double sum = 0.0;
        for (int acol = 0; acol < a -> ncols; acol++)
        {
          int aindex = (arow * a -> ncols) + acol;
          int bindex = (acol * b -> ncols) + bcol;
          sum += (a -> data[aindex] * b -> data[bindex]);
        }
        c -> data[count++] = sum;
      }
    }
  }
}