#include <stdio.h>
#include <string.h>

int main()
{
    FILE *readFile = fopen("./fruits.txt", "r");
    FILE *writeFile = fopen("./output.txt", "w");
    if (readFile == NULL || writeFile == NULL)
    {
        printf("Cannot open file");
        return 1;
    }

    char words[12][256];
    int index;
    while (!feof(readFile))
    {
        fscanf(readFile, "%s\n", &words[index++]);
    }

    for (int i = 0; i < sizeof(words) / sizeof(words[0]); i++)
    {
        char *word = words[i];
        char reversedWord[1024] = "";
        int count = 0;
        for (int j = strlen(word) - 1; j >= 0; j--)
            strncat(reversedWord, &word[j], 1);
        fprintf(writeFile, "%s\n", reversedWord);    
    }

    fclose(readFile);
    fclose(writeFile);
}