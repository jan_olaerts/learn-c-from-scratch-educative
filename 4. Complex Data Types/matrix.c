#include <stdio.h>

#define MAXDATA 1024

typedef struct
{
  double data[MAXDATA];
  int nrows;
  int ncols;
} Matrix;

void printmat(Matrix m);
Matrix matrixmult(Matrix a, Matrix b);

int main(int argc, char *argv[])
{
  Matrix a = {{1.2, 2.3, 3.4, 4.5, 5.6, 6.7}, 3, 2};
  Matrix b = {{5.5, 6.6, 7.7, 1.2, 2.1, 3.3}, 2, 3};
  Matrix c = matrixmult(a, b);
  printmat(c);

  return 0;
}

void printmat(Matrix matrix)
{
  int dataSize = matrix.nrows * matrix.ncols;
  for (int i = 0; i < dataSize; i++)
  {
    printf("matrix.data[%d]=%.1f\n", i, matrix.data[i]);
  }
  printf("\n");
}

Matrix matrixmult(Matrix a, Matrix b)
{
  if (a.nrows != b.ncols)
  {
    printf("a.nrows must be equal to b.ncols\n");
  }
  else
  {
    Matrix result;
    result.nrows = a.nrows;
    result.ncols = b.ncols;

    int count = 0;
    for (int arow = 0; arow < a.nrows; arow++)
    {
      for (int bcol = 0; bcol < b.ncols; bcol++)
      {
        double sum = 0.0;
        for (int acol = 0; acol < a.ncols; acol++)
        {
          int aindex = (arow * a.ncols) + acol;
          int bindex = (acol * b.ncols) + bcol;
          sum += (a.data[aindex] * b.data[bindex]);
        }
        result.data[count++] = sum;
      }
    }

    return result;
  }
}