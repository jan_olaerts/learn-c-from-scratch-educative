#include <stdio.h>
#include <string.h>

char *toUpperCase(char *text)
{
    int index = 0;
    while (text[index] != 0)
    {
        if (text[index] >= 'a' && text[index] <= 'z')
            text[index] = text[index] - 32;
        index++;
    }
    return text;
}