#include <stdio.h>

double degF_to_degC(double degF)
{
  double degC = (degF - 32) / 1.8;
  return degC;
}

void main()
{
  printf("27 °F = %.2f °C\n", degF_to_degC(27.0));
}