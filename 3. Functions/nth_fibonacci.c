#include <stdio.h>
#include <stdlib.h>

int fib(int n)
{
  if (n == 0)
    return 0;
  else if (n == 1)
    return 1;
  return n = fib(n - 1) + fib(n - 2);
}

void main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf("You must pass a number\n");
  }
  else if (argc > 2)
  {
    printf("You can only pass one argument\n");
  }
  else
  {
    printf("%d\n", fib(atoi(argv[1])));
  }
}