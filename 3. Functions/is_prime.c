#include <stdio.h>

int isPrime(int n)
{
  int count = n - 1;
  while (count > 1)
  {
    if (n % count == 0)
      return 0;
    count--;
  }

  return 1;
}

void main()
{
  printf("%d\n", isPrime(17));
}