#include <stdio.h>

int isPrime(int n)
{
  if (n == 0 || n == 1)
    return 0;
  int count = n - 1;
  while (count > 1)
  {
    if (n % count == 0)
      return 0;
    count--;
  }

  return 1;
}

int getNthPrime(int term)
{
  int lastPrime = 0;
  int count = 0;
  while (term > 0)
  {
    if (isPrime(count))
    {
      term--;
      lastPrime = count;
    }
    count++;
  }

  return lastPrime;
}

void main()
{
  printf("%d\n", getNthPrime(1));
  printf("%d\n", getNthPrime(2));
  printf("%d\n", getNthPrime(3));
  printf("%d\n", getNthPrime(4));
  printf("%d\n", getNthPrime(5));
  printf("%d\n", getNthPrime(996));
  printf("%d\n", getNthPrime(997));
  printf("%d\n", getNthPrime(998));
  printf("%d\n", getNthPrime(999));
  printf("%d\n", getNthPrime(1000));
}