#include <stdio.h>
#include <string.h>

void printCharacterNTimes(int times, char* character)
{
  while (times > 0)
  {
    printf("%s", character);
    times--;
  }
}

void drawPyramid(int height)
{
  int len = (height * 2) - 1;
  for (int row = 1; row <= height; row++)
  {
    char stars[100] = "";
    int numOfStars = (row * 2) - 1;
    int numOfSpaces = (len - numOfStars) / 2;
    printCharacterNTimes(numOfSpaces, " ");
    printCharacterNTimes(numOfStars, "*");
    printf("%s", "\n");
  }
}

void main()
{
  drawPyramid(6);
}